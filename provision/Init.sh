#!/bin/bash

# @TODO :
# - Create a config file to set environment var.
# - Create a user for minetest.
# - Set a default configuration and a way to add user configuration.

# Update system.
function __update()
{
    echo "[ INFO ] - Start update system"
    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get dist-upgrade -y
    echo "[ INFO ] - End update system"
}

# Install needed tools.
function __install_utils()
{
    sudo apt-get install -y wget\
                            git\
                            vim
}

# Install minetest server.
function __install_minetest_server()
{
    sudo apt-get install minetest-server -y
}

# Install monitor tool to check VM status.
function __install_monitor_tool()
{
    echo "[ INFO ] - Start install monitor tool"
    sudo apt-get install -y htop\
                            sysstat\
                            iftop
    echo "[ INFO ] - End install monitor tool"
}

# Install Custom function in .bashrc.
function __custom_bashrc()
{
    local directory_path="/home/vagrant/.local"
    local filename="custom_command.sh"
    local bashrc="/home/vagrant/.bashrc"

    # If directory not exist : create it.
    if [ ! -d $directory_path ];then
        mkdir $directory_path
    fi

    # Insert files with custom command.
    if [ -d $directory_path ];then
        cp -r /vagrant/provision/files/custom_command.sh $directory_path
        if [ -f $directory_path/$filename ];then
            echo "[ INFO ][ SUCCESS ] - file $directory_path/$filename as been moved"
        else
            echo "[ INFO ][ FAILED ] - An error occured during $directory_path/$filename move"
        fi
    else
        echo "[ INFO ][ FAILED ] - An error occured during $directory_path creation"
    fi

    # Update directory owner.
    sudo chown -R vagrant:vagrant $directory_path

    # Insert custom command file source in ubuntu .bashrc
    if [ -f $bashrc ];then
        if ! grep -Fxq "source $directory_path/$filename" $bashrc ;then
            echo "source $directory_path/$filename" >> $bashrc
        fi
    else
        touch $bashrc
        chown vagrant:vagrant $bashrc
        if ! grep -Fxq "source $directory_path/$filename" $bashrc ;then
            echo "source $directory_path/$filename" >> $bashrc
        fi
    fi
}

# Insert minetest config dir.
function __minetest_config_dir()
{
    local minetest_config_source="/vagrant/provision/minetest_config_server"
    local target_dir="/home/vagrant"

    # Check if source dir exist.
    if [ -d $minetest_config_source ];then
        cp -R $minetest_config_source $target_dir
        if [ -d $target_dir/minetest_config_server ];then
            echo "[ INFO ][ SUCCESS ] - directory $minetest_config_source as been moved to $target_dir"
        else
            echo "[ INFO ][ FAILED ] - An error occured during $minetest_config_source move"
        fi
    else
        echo "[ INFO ][ FAILED ] - directory $minetest_config_source does not exist"
    fi

    sudo chown -R vagrant:vagrant $target_dir/minetest_config_server
}

# Install texture pack.
# Texture pack come from @framasoft.
function __install_texture_pack()
{
    local texture_path="/home/vagrant/.minetest/textures"
    local textures_pack_url="https://framinetest.org/dl/textures.tar.gz"

    if ! -d $texture_path;then
        mkdir $texture_path
        chown vagrant:vagrant $texture_path
    fi

    cd $texture_path
    wget $textures_pack_url

    local file="$(find . -name "*.tar.gz")"
    if ! -z $file;then
        tar -xvf $file
        rm -fv $file
    fi
}

# Install mods from framasoft.
function __install_mods()
{
    local worldname="JDLL"
    local world_path="/home/vagrant/.minetest/worlds/$worldname"
    local framamods_url="https://framinetest.org/dl/worldmods.tar.gz"

    if [ ! -d $world_path ] ;then
        mkdir -p $world_path
    fi

    cd $world_path
    wget $framamods_url
    local file="$(find . -name "*.tar.gz")"
    if ! -z $file;then
        tar -xvf $file
        rm -fv $file
    fi

    # Auto remove unwork mod
    if [ -d $world_path/worldmods/car_f1/ ];then
        rm -rfv $world_path/worldmods/car_f1/
    fi
}

function __manage_system_file_access()
{
    sudo chown -R vagrant: /home/vagrant
}

# Exec all function here.
function main()
{
    echo "[ INFO ] - Start Initialisation of VM"
    __update
    __install_monitor_tool
    __install_utils
    __install_minetest_server
    __custom_bashrc
    __minetest_config_dir
    __install_texture_pack
    __install_mods
    __manage_system_file_access
    echo "[ INFO ] - End Initialisation of VM"
}

main
