# jdll_minetest_2018

# DESCRIPTION
make a  out of the box minetest server

with the framasoft configuration
https://framasoft.org/

in collaboration with ALDIL
http://www.aldil.org/

# TODO
[ ] Migrate from bash to ansible provision

## REQUIRED PACKAGES
- vagrant
- virtualbox
- git

```shell
git clone https://framagit.org/vdesbiez/jdll_minetest_2018.git
vagrant up --provider=virtualbox --provision
```

## Start minetest server
the host computer must be connected to a network
```shell
vagrant ssh
cd ~/minetest_config_server
minetestserver --config minetest_config_prod.conf
```

## Connection to the minetest server
```shell
vagrant ssh
ifconfig
```shell
vagrant ssh
cd ~/minetest_config_server
Insert server IP in minetest client with port 30006
```
